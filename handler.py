import datetime
import json
import pytz


def get_date_in_multiple_timezone(event, context):
    result = {}
    zone_names = pytz.all_timezones
    current_date_time = datetime.datetime.now()
    for zone in zone_names:
        result[zone] = pytz.timezone(zone).localize(current_date_time).isoformat()
    return json.dumps(result, indent=4, sort_keys=True, default=str)

